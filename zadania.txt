System do analizy zachowania człowieka w pomieszczeniu


Zdefiniowanie czynności ruchowych. Rejestracja czynności ruchowych, zbudowanie struktury bazy danych wraz z opisem zarejestrowanych próbek.
Przegląd dostępnych metod do rozpoznawania aktywności ruchowych człowieka (na podstawie analizy ruchu z akcelerometru).
Implementacja algorytmu rozpoznawania czynności ruchowych - tj. ekstrakcja cech i przyporządkowanie najlepiej pasującej czynności ruchowej. 
Implementacja aplikacji webowej (specyfikacja do dogadania).
Przegląd dostępnych metod do rozpoznawania komend głosowych. Zdefiniowanie komend głosowych do poszczególnych czynności ruchowych oraz komend pomocniczych do sterowania aplikacją. 
Implementacja algorytmu rozpoznawania komend głosowych - tj. ekstrakcja cech i przyporządkowanie najlepiej pasującej komendy do wypowiedzianej próbki głosowej. Implementacja aplikacji mobilnej.

Nagranie próbek głosowych - wszyscy.